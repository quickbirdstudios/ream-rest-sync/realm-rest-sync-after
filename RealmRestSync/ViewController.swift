//
//  ViewController.swift
//  RealmRestSync
//
//  Created by Stefan Kofler on 26.05.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    private var realm = try! Realm()

    fileprivate var users: [User] = []
    private var token: Any?

    @IBOutlet fileprivate var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let users = realm.objects(User.self)
        token = users.observe { _ in
            self.users = Array(self.realm.objects(User.self))
            self.tableView.reloadData()
        }
    }

    @IBAction private func addRandomUser() {
        let randomInt = Int(arc4random())

        let user = User()
        user.id = randomInt
        user.username = String(randomInt)

        try! realm.write {
            realm.add(user)
        }
    }

}

extension ViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let user = users[indexPath.row]

        cell.textLabel?.text = user.username
        cell.detailTextLabel?.text = String(describing: user.updatedDate)

        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

}

extension ViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = users[indexPath.row]

        try! realm.write {
            user.updatedDate = Date()
            realm.add(user, update: true)
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        let user = users[indexPath.row]

        try! realm.write {
            realm.delete(user)
        }
    }

}
